# Bachelor thesis template

A heavily customized template for bachelor thesis in maths at the University of Bonn.

I just extracted this from the code of my own thesis in the hope that it will be useful
for others.

# License
Note that the license does not apply to the submodule,
which is LPPLv3 licensed, see the corresponding repository.

Also note that `custom-styles/bachelor-titlepage.sty` is taken from
[https://www.mathematics.uni-bonn.de/files/bachelor/ba_titelseite_latex.zip](https://www.mathematics.uni-bonn.de/files/bachelor/ba_titelseite_latex.zip).
