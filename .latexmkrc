ensure_path('TEXINPUTS', './LatexPackagesBuild//', './custom-styles');  # set texinputs to find custom packages
$pdf_mode = 1;                                       # generate a pdf file by default
$out_dir = 'build';
